FROM openjdk:11
ADD target/discovery-service-0.0.1-SNAPSHOT.jar discovery-service
ENTRYPOINT ["java","-jar","discovery-service"]
